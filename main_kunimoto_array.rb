# encoding: utf-8

# ファイルを開く関数
def fileopen(fname)
    h = []
    File.open(fname, 'r:utf-8') do |f|
        while line = f.gets
            sa = line.split(' ')
            h.push(sa) if sa[0]
        end
    end
    h
end

price = fileopen('./price.txt')
shares = fileopen('./shares.txt')
ticker = fileopen('./ticker.txt')

out = []
price.each do |pr|
    tmp = [pr[0], pr[1]]
    shares.each { |val| tmp.push(val[1]) if val[0] == pr[0] }
    ticker.each { |val| tmp.push(val[1]) if val[0] == pr[0] }
    out.push(tmp) if tmp.length == 4
end

p out
