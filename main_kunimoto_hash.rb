# encoding: utf-8

# ファイルを開く関数
def fileopen(fname)
    h = {}
    File.open(fname, 'r:utf-8') do |f|
        while line = f.gets
            sa = line.split(' ')
            h[sa[0]] = sa[1] if sa[0]
        end
    end
    h
end

price = fileopen('./price.txt')
shares = fileopen('./shares.txt')
ticker = fileopen('./ticker.txt')

out = {}
price.each do |time, val|
    out[time] = {}
    out[time][:price] = val
    out[time][:shares] = shares[time]
    out[time][:ticker] = ticker[time]
end

p out
